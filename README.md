# Portfolio Projects
In this section I will list data analytics projects briefly describing the technology stack used to solve cases.

Some of the cases are simulations of real-life scenarios, while others come from open data sources or cources.

***
## Contents

- Reporting
- E-commerce project (Karpov courses) - RU

***

## Reporting

### A lightweight solution for reporting KPIs in Messenger

**Case:** It is required to produce report every 3 days that tracks the average KPIs of the 'X' app with a % difference from the previous period. The report should count in-game events to allow all stakeholders to make informed decisions regarding current performance. If a notable decrease in metrics is highlighted within the report, stakeholders should initiate investigations into the root causes.

Main points:
"Previous period" should count in-game event's schdeule and be set by analyst.The report should be easy to read and load quickly from any device.
App descriptions:
It's a game app. Performance metrics exhibit significant fluctuations influenced by ongoing in-game event.In-game event description:
A 21-day duration event with differents tasks for each day.

KPIs:
- ARPDAU - Average Revenue per Daily Active Users
- Revenue - Daily Revenue

**Clients:** Senior managers from multiple departments

**Solution:** Text report

1. [X] The eye can quickly screen it;
2. [X] The format loads quickly and does not crash;
3. [X] It can be sent to any messenger;
4. [X] Reports immediately state if the indicators are stable or if there has been a change;
   5. Specialists can also comment and indicate preliminary reasons for changes in indicators;
6. [X] The comparison period is also shown.

**Code:** [Link](A lightweight solution for reporting KPIs in Messenger.ipynb)

**Stack:** Python (Pandas), SQL Impala (Hadoop): impala.dbapi, os, pandas

**Type:** Real case

**Dataset:** Simulation

## E-commerce project (Karpov courses) - RU

**Case:** Study cases It is required to produce a dashboard that tracks dynamics of Daily Revenue with a % difference from the previous period.

**Code:** [Link](Проект e-commerce.ipynb)

**Stack:** Python (pandas, numpy, datetime, matplotlib.pyplot)

**Type:** Courses

**Data:** Karpov.courses